from django.urls import path, include
from django.contrib import admin

urlpatterns = [
    # Your URLs...
    path('admin/', admin.site.urls),
    path('api/authentication/', include('api.urls')),
]